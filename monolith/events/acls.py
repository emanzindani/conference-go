from .keys import PEXELS_API_KEY, OPEN_WEATHER_API_KEY
import json
import requests
def get_photo(city, state):
    headers = {"Authorization": PEXELS_API_KEY}
    url = f"https://api.pexels.com/v1/search?query={city}+{state}"
    response = requests.get(url, headers=headers)
    return response.json()["photos"][0]["url"]




def get_weather_data(city,state):
    # Get the lat and ln of the city
    params = {"q": f"{city}, {state},US",
    "appid": OPEN_WEATHER_API_KEY
    }
    url = "http://api.openweathermap.org/geo/1.0/direct"
    response = requests.get(url, params=params)
    content = response.json()
    try:
        latitude = content[0]["lat"]
        longitude = content[0]["lon"]
    except(KeyError, IndexError):
        return None

    # Get the weather
    params = {
        "lat": latitude,
        "lon": longitude,
        "unit": "imperial",
        "appid": OPEN_WEATHER_API_KEY
    }
    url = "https://api.openweathermap.org/data/2.5/weather"
    response = requests.get(url, params=params)
    json_resp = response.json()
    try:
        temp = json_resp["main"]["temp"]
        desc = json_resp["weather"][0]["description"]
        return {"temp": temp, "description": desc}
    except(KeyError, IndexError):
        return None
